package com.neuedu.mapper;

import com.neuedu.AppTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/7/28 09:02
 * @version: 1.0.0
 * @Description:
 */
@Slf4j
public class AccountMapperTest extends AppTest {
    @Autowired
    private AccountMapper accountMapper;
    @Test
    public void testMapper(){
        // log.info(accountMapper.toString());
        Map map = new HashMap<>();
        map.put("id", 1);
        map.put("money", new BigDecimal(-500));
        accountMapper.update(map);
    }

}