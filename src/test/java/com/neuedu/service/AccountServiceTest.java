package com.neuedu.service;

import com.neuedu.AppTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @Author: MARK
 * @Date: 2019/7/28 09:15
 * @version: 1.0.0
 * @Description:
 */
@Slf4j
public class AccountServiceTest extends AppTest {
    @Autowired
    private AccountService accountService;
    @Test
    public void testService(){
        accountService.transfer(1, 2, new BigDecimal(500));
    }

}