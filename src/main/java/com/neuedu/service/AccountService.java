package com.neuedu.service;

import java.math.BigDecimal;

/**
 * @Author: MARK
 * @Date: 2019/7/27 16:07
 * @version: 1.0.0
 * @Description:
 */
public interface AccountService {
    //谁给谁转了多少钱
    void transfer(Integer id1, Integer id2, BigDecimal money);
}
