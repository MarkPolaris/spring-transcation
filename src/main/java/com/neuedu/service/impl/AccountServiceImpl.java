package com.neuedu.service.impl;

import com.neuedu.mapper.AccountMapper;
import com.neuedu.service.AccountService;
import jdk.management.resource.internal.inst.UnixAsynchronousServerSocketChannelImplRMHooks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: MARK
 * @Date: 2019/7/27 16:07
 * @version: 1.0.0
 * @Description:
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountMapper accountMapper;
    @Override
    public void transfer(Integer id1, Integer id2, BigDecimal money) {
        //两个账户分别更新
        //账户1减钱
        Map map1 = new HashMap<>();
        map1.put("id", id1);
        map1.put("money", money.multiply(new BigDecimal(-1)));
        accountMapper.update(map1);

        int i = 1/0;

        //账户2加钱
        Map map2 = new HashMap<>();
        map2.put("id", id2);
        map2.put("money", money);
        accountMapper.update(map2);
    }
}
