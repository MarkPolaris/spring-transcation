package com.neuedu.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: MARK
 * @Date: 2019/7/27 16:07
 * @version: 1.0.0
 * @Description:
 */
@Data
public class Account {
    private Integer id;
    private String name;
    private BigDecimal money;
}
