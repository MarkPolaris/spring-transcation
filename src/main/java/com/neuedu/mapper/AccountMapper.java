package com.neuedu.mapper;

import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @Author: MARK
 * @Date: 2019/7/27 16:07
 * @version: 1.0.0
 * @Description:
 */
@Repository
public interface AccountMapper {
    //更新该账户的金额
    void update(Map map);
}
