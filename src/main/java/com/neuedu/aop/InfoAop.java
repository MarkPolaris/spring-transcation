package com.neuedu.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Author: MARK
 * @Date: 2019/7/28 09:32
 * @version: 1.0.0
 * @Description:
 */
@Component
@Aspect
@Slf4j
public class InfoAop {
    //切入点
    @Pointcut("execution(* com.neuedu.*..*(..))")
    private void anyMethod(){}

    @Around(value = "anyMethod()")
    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        String clazz = proceedingJoinPoint.getTarget().getClass().getName();
        String method = proceedingJoinPoint.getSignature().getName();
        String args = JSON.toJSONString(proceedingJoinPoint.getArgs());
        proceedingJoinPoint.proceed();
        log.info("当前类" + clazz +
                "\n执行方法" + method +
                "\n参数为" + args
                );
    }
}
